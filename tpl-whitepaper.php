<?php

/* Template name: Landing Page Whitepaper */

if(have_posts()):while(have_posts()):the_post(); ?>

<!DOCTYPE html><html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>BoaCompra - Business Expansion into LATAM</title>
  <link rel="icon" href="favicon.png" />
  <link rel="stylesheet" href="https://use.typekit.net/mrj8ybr.css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style-bc.css">
  <meta name="description" content="Download our Whitepaper on Business Expansion and payment methods in LATAM">
</head>
<body>

<main>
  <section class="top">
    <div class="container">
      <h1 class="logo">
        <a href="http://boacompra.com" target="_blank">BoaCompra - By Pagseguro</a>
      </h1>
      <div class="intro">
        <div class="intro-wrap">
          <h2 class="intro-title">Business expansion into Latin America</h2>
          <div class="intro-text">
            <p>Mauris vulputate, mi nec porttitor lobortis, est massa blandit diam, non hendrerit nisi sapien egestas nisl. Mauris ultricies lacus tincidunt gravida lobortis.</p>
            <p>Suspendisse rhoncus sed enim et imperdiet. In ornare velit sed efficitur porta. Sed euismod, nisl sed tincidunt suscipit, orci tellus eleifend est, eu ultricies purus arcu in sapien. Nulla vitae ultricies ante, sit amet imperdiet felis. Nunc malesuada ac lectus id elementum.</p>
          </div>
        </div>
      </div>
      <figure class="intro-image"></figure>
    </div>
  </section>
  <section class="numbers" data-js="Numbers">
    <div class="container">
      <h2 class="numbers-title">Did you know that</h2>
      <ul class="numbers-list">
        <div class="numbers-wrap">
          <li class="number">
            <div class="number-chart">
              <span class="number-label" data-number="63%"><span class="num">63</span>%</span>
              <svg viewBox="0 0 36 36" class="svgAnimate">
                <path
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke="#DFD1A7";
                  stroke-width="3";
                  stroke-dasharray="100, 100"
                />
                <path
                  data-number="63"
                  class="animate"
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke="#E35205";
                  stroke-width="3";
                  stroke-dasharray="63, 100"
                />
              </svg>
            </div>
            <div class="number-text">
              <p>Of the Mexican population don’t have a bank account. (2017)</p>
            </div>
          </li>
          <li class="number">
            <div class="number-chart">
              <span class="number-label"><span class="num">22</span>%</span>
              <svg viewBox="0 0 36 36" class="svgAnimate">
                <path
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke="#DFD1A7";
                  stroke-width="3";
                  stroke-dasharray="100, 100"
                />
                <path
                  data-number="22"
                  class="animate"
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke="#E35205";
                  stroke-width="3";
                  stroke-dasharray="22, 100"
                />
              </svg>
            </div>
            <div class="number-text">
              <p>Of credit cards in Brazil are enabled for international transactions.</p>
            </div>
          </li>
          <li class="number">
            <div class="number-chart">
              <span class="number-label"><span class="num">45.8</span>%</span>
              <svg viewBox="0 0 36 36" class="svgAnimate">
                <path
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke="#DFD1A7";
                  stroke-width="3";
                  stroke-dasharray="100, 100"
                />
                <path
                  data-number="46"
                  class="animate"
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke="#E35205";
                  stroke-width="3";
                  stroke-dasharray="46, 100"
                />
              </svg>
            </div>
            <div class="number-text">
              <p>Of e-commerce sales in Brazil were made in installments. (2018)</p>
            </div>
          </li>
          <li class="number">
            <div class="number-chart">
              <span class="number-label"><span class="num">73.5</span>%</span>
              <svg viewBox="0 0 36 36" class="svgAnimate">
                <path
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke="#DFD1A7";
                  stroke-width="3";
                  stroke-dasharray="100, 100"
                />
                <path
                  data-number="74"
                  class="animate"
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke="#E35205";
                  stroke-width="3";
                  stroke-dasharray="74, 100"
                />
              </svg>
            </div>
            <div class="number-text">
              <p>Of the Chilean population have access to the internet. (2018)</p>
            </div>
          </li>
        </div>
      </ul>
    </div>
  </section>
  <section class="cta">
    <div class="container">
      <div class="cta-wrap">
        <h2 class="cta-title">Expand your Business Now</h2>
        <div class="cta-content">
          <div class="cta-text">
            <p>To access the ebook, fill in the fields.</p>
          </div>
          <script src="//app-ab09.marketo.com/js/forms2/js/forms2.min.js"></script>
          <form id="mktoForm_1960"></form>
          <script>MktoForms2.loadForm("//app-ab09.marketo.com", "617-ZXF-679", 1960);</script>
        </div>
      </div>
      <figure class="whitepaper-mockup">
        <img src="<?php bloginfo('template_directory'); ?>/img/BusinessExpansionIntoLATAM-Mockup.png" alt="BoaCompra's Whitepaper Mockup">
      </figure>
    </div>
  </section>
  <footer>
    <div class="container">
      <p class="copy">Copyright 2018 UOL BoaCompra &copy; All rights reserved</p>
    </div>
  </footer>
</main>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-30834803-38"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-30834803-38');
</script>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/script.js"></script>
</body>
</html>
<?php endwhile; endif; ?>
